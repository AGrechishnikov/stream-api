import java.util.*;
import java.util.function.Function;
import java.util.stream.*;
import java.nio.charset.StandardCharsets;



public class StreamAPIUtil {

    private static List<String> WordOccurrence(ArrayList<String> list){
        return list.stream()
        .map(text ->text.toLowerCase())
        .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
        .entrySet()
        .stream()
        .sorted(Map.Entry.<String, Long>comparingByValue().reversed().thenComparing(Map.Entry::getKey))
        .map(Map.Entry::getKey)
        .limit(10).collect(Collectors.toList());

    }


    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in, StandardCharsets.UTF_8);
        String text = scan.nextLine();
        ArrayList<String> words = new ArrayList<String>(Arrays.asList(text.replaceAll("[^A-Za-z�-��-�-\s]","").split(" ")));
        List<String> temp=WordOccurrence(words);
        for(String word: temp){
            System.out.println(word);
        }
    }
}
